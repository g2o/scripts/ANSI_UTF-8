local utf8_windows1258_map =
{
	["\xe2\x82\xac"] = "\x80", // "€"
	["\xe2\x80\x9a"] = "\x82", // "‚"
	["\xc6\x92"] = "\x83", // "ƒ"
	["\xe2\x80\x9e"] = "\x84", // "„"
	["\xe2\x80\xa6"] = "\x85", // "…"
	["\xe2\x80\xa0"] = "\x86", // "†"
	["\xe2\x80\xa1"] = "\x87", // "‡"
	["\xcb\x86"] = "\x88", // "ˆ"
	["\xe2\x80\xb0"] = "\x89", // "‰"
	["\xe2\x80\xb9"] = "\x8b", // "‹"
	["\xc5\x92"] = "\x8c", // "Œ"

	["\xe2\x80\x98"] = "\x91", // "‘"
	["\xe2\x80\x99"] = "\x92", // "’"
	["\xe2\x80\x9c"] = "\x93", // "“"
	["\xe2\x80\x9d"] = "\x94", // "”"
	["\xe2\x80\xa2"] = "\x95", // "•"
	["\xe2\x80\x93"] = "\x96", // "–"
	["\xe2\x80\x94"] = "\x97", // "—"
	["\xcb\x9c"] = "\x98", // "˜"
	["\xe2\x84\xa2"] = "\x99", // "™"
	["\xe2\x80\xba"] = "\x9b", // "›"
	["\xc5\x93"] = "\x9c", // "œ"
	["\xc5\xb8"] = "\x9f", // "Ÿ"

	["\x20"] = "\xa0", // NBSP
	["\xc2\xa1"] = "\xa1", // "¡"
	["\xc2\xa2"] = "\xa2", // "¢"
	["\xc2\xa3"] = "\xa3", // "£"
	["\xc2\xa4"] = "\xa4", // "¤"
	["\xc2\xa5"] = "\xa5", // "¥"
	["\xc2\xa6"] = "\xa6", // "¦"
	["\xc2\xa7"] = "\xa7", // "§"
	["\xc2\xa8"] = "\xa8", // "¨"
	["\xc2\xa9"] = "\xa9", // "©"
	["\xc2\xaa"] = "\xaa", // "ª"
	["\xc2\xab"] = "\xab", // "«"
	["\xc2\xac"] = "\xac", // "¬"
	["\xc2\xad"] = "\xad", // SHY
	["\xc2\xae"] = "\xae", // "®"
	["\xc2\xaf"] = "\xaf", // "¯"

	["\xc2\xb0"] = "\xb0", // "°"
	["\xc2\xb1"] = "\xb1", // "±"
	["\xc2\xb2"] = "\xb2", // "²"
	["\xc2\xb3"] = "\xb3", // "³"
	["\xc2\xb4"] = "\xb4", // "´"
	["\xc2\xb5"] = "\xb5", // "µ"
	["\xc2\xb6"] = "\xb6", // "¶"
	["\xc2\xb7"] = "\xb7", // "·"
	["\xc2\xb8"] = "\xb8", // "¸"
	["\xc2\xb9"] = "\xb9", // "¹"
	["\xc2\xba"] = "\xba", // "º"
	["\xc2\xbb"] = "\xbb", // "»"
	["\xc2\xbc"] = "\xbc", // "¼"
	["\xc2\xbd"] = "\xbd", // "½"
	["\xc2\xbe"] = "\xbe", // "¾"
	["\xc2\xbf"] = "\xbf", // "¿"

	["\xc3\x80"] = "\xc0", // "À"
	["\xc3\x81"] = "\xc1", // "Á"
	["\xc3\x82"] = "\xc2", // "Â"
	["\xc4\x82"] = "\xc3", // "Ă"
	["\xc3\x84"] = "\xc4", // "Ä"
	["\xc3\x85"] = "\xc5", // "Å"
	["\xc3\x86"] = "\xc6", // "Æ"
	["\xc3\x87"] = "\xc7", // "Ç"
	["\xc3\x88"] = "\xc8", // "È"
	["\xc3\x89"] = "\xc9", // "É"
	["\xc3\x8a"] = "\xca", // "Ê"
	["\xc3\x8b"] = "\xcb", // "Ë"
	["\xe2\x97\x8c\xcc\x80"] = "\x3f\xcc", // "◌̀"
	["\xc3\x8d"] = "\xcd", // "Í"
	["\xc3\x8e"] = "\xce", // "Î"
	["\xc3\x8f"] = "\xcf", // "Ï"

	["\xc4\x90"] = "\xd0", // "Đ"
	["\xc3\x91"] = "\xd1", // "Ñ"
	["\xe2\x97\x8c\xcc\x89"] = "\x3f\xd2", // "◌̉"
	["\xc3\x93"] = "\xd3", // "Ó"
	["\xc3\x94"] = "\xd4", // "Ô"
	["\xc6\xa0"] = "\xd5", // "Ơ"
	["\xc3\x96"] = "\xd6", // "Ö"
	["\xc3\x97"] = "\xd7", // "×"
	["\xc3\x98"] = "\xd8", // "Ø"
	["\xc3\x99"] = "\xd9", // "Ù"
	["\xc3\x9a"] = "\xda", // "Ú"
	["\xc3\x9b"] = "\xdb", // "Û"
	["\xc3\x9c"] = "\xdc", // "Ü"
	["\xc6\xaf"] = "\xdd", // "Ư"
	["\xe2\x97\x8c\xcc\x83"] = "\x3f\xde", // "◌̃"
	["\xc3\x9f"] = "\xdf", // "ß"

	["\xc3\xa0"] = "\xe0", // "à"
	["\xc3\xa1"] = "\xe1", // "á"
	["\xc3\xa2"] = "\xe2", // "â"
	["\xc4\x83"] = "\xe3", // "ă"
	["\xc3\xa4"] = "\xe4", // "ä"
	["\xc3\xa5"] = "\xe5", // "å"
	["\xc3\xa6"] = "\xe6", // "æ"
	["\xc3\xa7"] = "\xe7", // "ç"
	["\xc3\xa8"] = "\xe8", // "è"
	["\xc3\xa9"] = "\xe9", // "é"
	["\xc3\xaa"] = "\xea", // "ê"
	["\xc3\xab"] = "\xeb", // "ë"
	["\xe2\x97\x8c\xcc\x81"] = "\x3f\xec", // "◌́"
	["\xc3\xad"] = "\xed", // "í"
	["\xc3\xae"] = "\xee", // "î"
	["\xc3\xaf"] = "\xef", // "ï"

	["\xc4\x91"] = "\xf0", // "đ"
	["\xc3\xb1"] = "\xf1", // "ñ"
	["\xe2\x97\x8c\xcc\xa3"] = "\x3f\xf2", // "◌̣"
	["\xc3\xb3"] = "\xf3", // "ó"
	["\xc3\xb4"] = "\xf4", // "ô"
	["\xc6\xa1"] = "\xf5", // "ơ"
	["\xc3\xb6"] = "\xf6", // "ö"
	["\xc3\xb7"] = "\xf7", // "÷"
	["\xc3\xb8"] = "\xf8", // "ø"
	["\xc3\xb9"] = "\xf9", // "ù"
	["\xc3\xba"] = "\xfa", // "ú"
	["\xc3\xbb"] = "\xfb", // "û"
	["\xc3\xbc"] = "\xfc", // "ü"
	["\xc6\xb0"] = "\xfd", // "ư"
	["\xe2\x82\xab"] = "\xfe", // "₫"
	["\xc3\xbf"] = "\xff", // ÿ
}

function utf8_to_windows1258(text)
{
	local result = ""

	for (local i = 0, charSize = 0, textLen = text.len(); i != textLen; i += charSize)
	{
		local char = text[i]

		if (char < 0)
			char += 256

		if(char < 128)
			charSize = 1
		else
		{
			if(char < 224)
				charSize = 2
			else if(char < 240)
				charSize = 3
			else if(char < 248)
				charSize = 4
			else if(char == 252)
			charSize = 5
			else
				charSize = 6
		}

		local charBytes = text.slice(i, i + charSize)

		if (charSize == 1)
			result += charBytes
		else if (charBytes in utf8_windows1258_map)
			result += utf8_windows1258_map[charBytes]
		else
			result += "?"
	}

	return result
}