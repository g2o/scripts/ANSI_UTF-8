local windows1256_utf8_map =
{
	[0x80] = "\xe2\x82\xac", // "€"
	[0x81] = "\xd9\xbe", // "پ"
	[0x82] = "\xe2\x80\x9a", // "‚"
	[0x83] = "\xc6\x92", // "ƒ"
	[0x84] = "\xe2\x80\x9e", // "„"
	[0x85] = "\xe2\x80\xa6", // "…"
	[0x86] = "\xe2\x80\xa0", // "†"
	[0x87] = "\xe2\x80\xa1", // "‡"
	[0x88] = "\xcb\x86", // "ˆ"
	[0x89] = "\xe2\x80\xb0", // "‰"
	[0x8a] = "\xd9\xb9", // "ٹ"
	[0x8b] = "\xe2\x80\xb9", // "‹"
	[0x8c] = "\xc5\x92", // "Œ"
	[0x8d] = "\xda\x86", // "چ"
	[0x8e] = "\xda\x98", // "ژ"
	[0x8f] = "\xda\x88", // "ڈ"

	[0x90] = "\xda\xaf", // "گ"
	[0x91] = "\xe2\x80\x98", // "‘"
	[0x92] = "\xe2\x80\x99", // "’"
	[0x93] = "\xe2\x80\x9c", // "“"
	[0x94] = "\xe2\x80\x9d", // "”"
	[0x95] = "\xe2\x80\xa2", // "•"
	[0x96] = "\xe2\x80\x93", // "–"
	[0x97] = "\xe2\x80\x94", // "—"
	[0x98] = "\xda\xa9", // "ک"
	[0x99] = "\xe2\x84\xa2", // "™"
	[0x9a] = "\xda\x91", // "ڑ"
	[0x9b] = "\xe2\x80\xba", // "›"
	[0x9c] = "\xc5\x93", // "œ"
	[0x9d] = "\xe2\x80\x8c", // ZWNJ
	[0x9e] = " \xe2\x80\x8d", // ZWJ
	[0x9f] = "\xda\xba", // "ں"

	[0xa0] = "\x20", // NBSP
	[0xa1] = "\xd8\x8c", // "،"
	[0xa2] = "\xc2\xa2", // "¢"
	[0xa3] = "\xc2\xa3", // "£"
	[0xa4] = "\xc2\xa4", // "¤"
	[0xa5] = "\xc2\xa5", // "¥"
	[0xa6] = "\xc2\xa6", // "¦"
	[0xa7] = "\xc2\xa7", // "§"
	[0xa8] = "\xc2\xa8", // "¨"
	[0xa9] = "\xc2\xa9", // "©"
	[0xaa] = "\xda\xbe", // "ھ"
	[0xab] = "\xc2\xab", // "«"
	[0xac] = "\xc2\xac", // "¬"
	[0xad] = "\xc2\xad", // SHY
	[0xae] = "\xc2\xae", // "®"
	[0xaf] = "\xc2\xaf", // "¯"

	[0xb0] = "\xc2\xb0", // "°"
	[0xb1] = "\xc2\xb1", // "±"
	[0xb2] = "\xc2\xb2", // "²"
	[0xb3] = "\xc2\xb3", // "³"
	[0xb4] = "\xc2\xb4", // "´"
	[0xb5] = "\xc2\xb5", // "µ"
	[0xb6] = "\xc2\xb6", // "¶"
	[0xb7] = "\xc2\xb7", // "·"
	[0xb8] = "\xc2\xb8", // "¸"
	[0xb9] = "\xc2\xb9", // "¹"
	[0xba] = "\xd8\x9b", // "؛"
	[0xbb] = "\xc2\xbb", // "»"
	[0xbc] = "\xc2\xbc", // "¼"
	[0xbd] = "\xc2\xbd", // "½"
	[0xbe] = "\xc2\xbe", // "¾"
	[0xbf] = "\xd8\x9f", // "؟"

	[0xc0] = "\xdb\x81", // "ہ"
	[0xc1] = "\xd8\xa1", // "ء"
	[0xc2] = "\xd8\xa2", // "آ"
	[0xc3] = "\xd8\xa3", // "أ"
	[0xc4] = "\xd8\xa4", // "ؤ"
	[0xc5] = "\xd8\xa5", // "إ"
	[0xc6] = "\xd8\xa6", // "ئ"
	[0xc7] = "\xd8\xa7", // "ا"
	[0xc8] = "\xd8\xa8", // "ب"
	[0xc9] = "\xd8\xa9", // "ة"
	[0xca] = "\xd8\xaa", // "ت"
	[0xcb] = "\xd8\xab", // "ث"
	[0xcc] = "\xd8\xac", // "ج"
	[0xcd] = "\xd8\xad", // "ح"
	[0xce] = "\xd8\xae", // "خ"
	[0xcf] = "\xd8\xaf", // "د"

	[0xd0] = "\xd8\xb0", // "ذ"
	[0xd1] = "\xd8\xb1", // "ر"
	[0xd2] = "\xd8\xb2", // "ز"
	[0xd3] = "\xd8\xb3", // "س"
	[0xd4] = "\xd8\xb4", // "ش"
	[0xd5] = "\xd8\xb5", // "ص"
	[0xd6] = "\xd8\xb6", // "ض"
	[0xd7] = "\xc3\x97", // "×"
	[0xd8] = "\xd8\xb7", // "ط"
	[0xd9] = "\xd8\xb8", // "ظ"
	[0xda] = "\xd8\xb9", // "ع"
	[0xdb] = "\xd8\xba", // "غ"
	[0xdc] = "\xd9\x80", // "ـ"
	[0xdd] = "\xd9\x81", // "ف"
	[0xde] = "\xd9\x82", // "ق"
	[0xdf] = "\xd9\x83", // "ك"

	[0xe0] = "\xc3\xa0", // "à"
	[0xe1] = "\xd9\x84", // "ل"
	[0xe2] = "\xc3\xa2", // "â"
	[0xe3] = "\xd9\x85", // "م"
	[0xe4] = "\xd9\x86", // "ن"
	[0xe5] = "\xd9\x87", // "ه"
	[0xe6] = "\xd9\x88", // "و"
	[0xe7] = "\xc3\xa7", // "ç"
	[0xe8] = "\xc3\xa8", // "è"
	[0xe9] = "\xc3\xa9", // "é"
	[0xea] = "\xc3\xaa", // "ê"
	[0xeb] = "\xc3\xab", // "ë"
	[0xec] = "\xd9\x89", // "ى"
	[0xed] = "\xd9\x8a", // "ي"
	[0xee] = "\xc3\xae", // "î"
	[0xef] = "\xc3\xaf", // "ï"

	[0xf0] = "\xd9\x8b", // "ً"
	[0xf1] = "\xd9\x8c", // "ٌ"
	[0xf2] = "\xd9\x8d", // "ٍ"
	[0xf3] = "\xd9\x8e", // "َ"
	[0xf4] = "\xc3\xb4", // "ô"
	[0xf5] = "\xd9\x8f", // "ُ"
	[0xf6] = "\xd9\x90", // "ِ"
	[0xf7] = "\xc3\xb7", // "÷"
	[0xf8] = "\xd9\x91", // "ّ"
	[0xf9] = "\xc3\xb9", // "ù"
	[0xfa] = "\xd9\x92", // "ْ"
	[0xfb] = "\xc3\xbb", // "û"
	[0xfc] = "\xc3\xbc", // "ü"
	[0xfd] = "\xe2\x80\x8e", // LRM
	[0xfe] = "\xe2\x80\x8f", // RLM
	[0xff] = "\xdb\x92", // "ے"
}

function windows1256_to_utf8(text)
{
	local result = ""

	foreach (idx, charId in text)
	{
		charId += 256

		if (charId in windows1256_utf8_map)
			result += windows1256_utf8_map[charId]
		else
			result += text.slice(idx, idx + 1)
	}

	return result
}