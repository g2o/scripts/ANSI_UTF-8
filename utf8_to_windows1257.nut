local utf8_windows1257_map =
{
	["\xe2\x82\xac"] = "\x80", // "€"
	["\xe2\x80\x9a"] = "\x82", // "‚"
	["\xe2\x80\x9e"] = "\x84", // "„"
	["\xe2\x80\xa6"] = "\x85", // "…"
	["\xe2\x80\xa0"] = "\x86", // "†"
	["\xe2\x80\xa1"] = "\x87", // "‡"
	["\xe2\x80\xb0"] = "\x89", // "‰"
	["\xe2\x80\xb9"] = "\x8b", // "‹"
	["\xc2\xa8"] = "\x8d", // "¨"
	["\xcb\x87"] = "\x8e", // "ˇ"
	["\xc2\xb8"] = "\x8f", // "¸"

	["\xe2\x80\x98"] = "\x91", // "‘"
	["\xe2\x80\x99"] = "\x92", // "’"
	["\xe2\x80\x9c"] = "\x93", // "“"
	["\xe2\x80\x9d"] = "\x94", // "”"
	["\xe2\x80\xa2"] = "\x95", // "•"
	["\xe2\x80\x93"] = "\x96", // "–"
	["\xe2\x80\x94"] = "\x97", // "—"
	["\xe2\x84\xa2"] = "\x99", // "™"
	["\xe2\x80\xba"] = "\x9b", // "›"
	["\xc2\xaf"] = "\x9d", // "¯"
	["\xcb\x9b"] = "\x9e", // "˛"

	["\x20"] = "\xa0", // NBSP
	["\xc2\xa2"] = "\xa2", // "¢"
	["\xc2\xa3"] = "\xa3", // "£"
	["\xc2\xa4"] = "\xa4", // "¤"
	["\xc2\xa6"] = "\xa6", // "¦"
	["\xc2\xa7"] = "\xa7", // "§"
	["\xc3\x98"] = "\xa8", // "Ø"
	["\xc2\xa9"] = "\xa9", // "©"
	["\xc5\x96"] = "\xaa", // "Ŗ"
	["\xc2\xab"] = "\xab", // "«"
	["\xc2\xac"] = "\xac", // "¬"
	["\xc2\xad"] = "\xad", // SHY
	["\xc2\xae"] = "\xae", // "®"
	["\xc3\x86"] = "\xaf", // "Æ"

	["\xc2\xb0"] = "\xb0", // "°"
	["\xc2\xb1"] = "\xb1", // "±"
	["\xc2\xb2"] = "\xb2", // "²"
	["\xc2\xb3"] = "\xb3", // "³"
	["\xc2\xb4"] = "\xb4", // "´"
	["\xc2\xb5"] = "\xb5", // "µ"
	["\xc2\xb6"] = "\xb6", // "¶"
	["\xc2\xb7"] = "\xb7", // "·"
	["\xc3\xb8"] = "\xb8", // "ø"
	["\xc2\xb9"] = "\xb9", // "¹"
	["\xc5\x97"] = "\xba", // "ŗ"
	["\xc2\xbb"] = "\xbb", // "»"
	["\xc2\xbc"] = "\xbc", // "¼"
	["\xc2\xbd"] = "\xbd", // "½"
	["\xc2\xbe"] = "\xbe", // "¾"
	["\xc3\xa6"] = "\xbf", // "æ"

	["\xc4\x84"] = "\xc0", // "Ą"
	["\xc4\xae"] = "\xc1", // "Į"
	["\xc4\x80"] = "\xc2", // "Ā"
	["\xc4\x86"] = "\xc3", // "Ć"
	["\xc3\x84"] = "\xc4", // "Ä"
	["\xc3\x85"] = "\xc5", // "Å"
	["\xc4\x98"] = "\xc6", // "Ę"
	["\xc4\x92"] = "\xc7", // "Ē"
	["\xc4\x8c"] = "\xc8", // "Č"
	["\xc3\x89"] = "\xc9", // "É"
	["\xc5\xb9"] = "\xca", // "Ź"
	["\xc4\x96"] = "\xcb", // "Ė"
	["\xc4\xa2"] = "\xcc", // "Ģ"
	["\xc4\xb6"] = "\xcd", // "Ķ"
	["\xc4\xaa"] = "\xce", // "Ī"
	["\xc4\xbb"] = "\xcf", // "Ļ"

	["\xc5\xa0"] = "\xd0", // "Š"
	["\xc5\x83"] = "\xd1", // "Ń"
	["\xc5\x85"] = "\xd2", // "Ņ"
	["\xc3\x93"] = "\xd3", // "Ó"
	["\xc5\x8c"] = "\xd4", // "Ō"
	["\xc3\x95"] = "\xd5", // "Õ"
	["\xc3\x96"] = "\xd6", // "Ö"
	["\xc3\x97"] = "\xd7", // "×"
	["\xc5\xb2"] = "\xd8", // "Ų"
	["\xc5\x81"] = "\xd9", // "Ł"
	["\xc5\x9a"] = "\xda", // "Ś"
	["\xc5\xaa"] = "\xdb", // "Ū"
	["\xc3\x9c"] = "\xdc", // "Ü"
	["\xc5\xbb"] = "\xdd", // "Ż"
	["\xc5\xbd"] = "\xde", // "Ž"
	["\xc3\x9f"] = "\xdf", // "ß"

	["\xc4\x85"] = "\xe0", // "ą"
	["\xc4\xaf"] = "\xe1", // "į"
	["\xc4\x81"] = "\xe2", // "ā"
	["\xc4\x87"] = "\xe3", // "ć"
	["\xc3\xa4"] = "\xe4", // "ä"
	["\xc3\xa5"] = "\xe5", // "å"
	["\xc4\x99"] = "\xe6", // "ę"
	["\xc4\x93"] = "\xe7", // "ē"
	["\xc4\x8d"] = "\xe8", // "č"
	["\xc3\xa9"] = "\xe9", // "é"
	["\xc5\xba"] = "\xea", // "ź"
	["\xc4\x97"] = "\xeb", // "ė"
	["\xc4\xa3"] = "\xec", // "ģ"
	["\xc4\xb7"] = "\xed", // "ķ"
	["\xc4\xab"] = "\xee", // "ī"
	["\xc4\xbc"] = "\xef", // "ļ"

	["\xc5\xa1"] = "\xf0", // "š"
	["\xc5\x84"] = "\xf1", // "ń"
	["\xc5\x86"] = "\xf2", // "ņ"
	["\xc3\xb3"] = "\xf3", // "ó"
	["\xc5\x8d"] = "\xf4", // "ō"
	["\xc3\xb5"] = "\xf5", // "õ"
	["\xc3\xb6"] = "\xf6", // "ö"
	["\xc3\xb7"] = "\xf7", // "÷"
	["\xc5\xb3"] = "\xf8", // "ų"
	["\xc5\x82"] = "\xf9", // "ł"
	["\xc5\x9b"] = "\xfa", // "ś"
	["\xc5\xab"] = "\xfb", // "ū"
	["\xc3\xbc"] = "\xfc", // "ü"
	["\xc5\xbc"] = "\xfd", // "ż"
	["\xc5\xbe"] = "\xfe", // "ž"
	["\xcb\x99"] = "\xff", // "˙"
}

function utf8_to_windows1257(text)
{
	local result = ""

	for (local i = 0, charSize = 0, textLen = text.len(); i != textLen; i += charSize)
	{
		local char = text[i]

		if (char < 0)
			char += 256

		if(char < 128)
			charSize = 1
		else
		{
			if(char < 224)
				charSize = 2
			else if(char < 240)
				charSize = 3
			else if(char < 248)
				charSize = 4
			else if(char == 252)
			charSize = 5
			else
				charSize = 6
		}

		local charBytes = text.slice(i, i + charSize)

		if (charSize == 1)
			result += charBytes
		else if (charBytes in utf8_windows1257_map)
			result += utf8_windows1257_map[charBytes]
		else
			result += "?"
	}

	return result
}