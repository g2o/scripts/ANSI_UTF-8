local utf8_windows1250_map =
{
	["\xe2\x82\xac"] = "\x80", // "€"
	["\xe2\x80\x9a"] = "\x82", // "‚"
	["\xe2\x80\x9e"] = "\x84", // "„"
	["\xe2\x80\xa6"] = "\x85", // "…"
	["\xe2\x80\xa0"] = "\x86", // "†"
	["\xe2\x80\xa1"] = "\x87", // "‡"
	["\xe2\x80\xb0"] = "\x89", // "‰"
	["\xc5\xa0"] = "\x8a", // "Š"
	["\xe2\x80\xb9"] = "\x8b", // "‹"
	["\xc5\x9a"] = "\x8c", // "Ś"
	["\xc5\xa4"] = "\x8d", // "Ť"
	["\xc5\xbd"] = "\x8e", // "Ž"
	["\xc5\xb9"] = "\x8f", // "Ź"

	["\xe2\x80\x98"] = "\x91", // "‘"
	["\xe2\x80\x99"] = "\x92", // "’"
	["\xe2\x80\x9c"] = "\x93", // "“"
	["\xe2\x80\x9d"] = "\x94", // "”"
	["\xe2\x80\xa2"] = "\x95", // "•"
	["\xe2\x80\x93"] = "\x96", // "–"
	["\xe2\x80\x94"] = "\x97", // "—"
	["\xe2\x84\xa2"] = "\x99", // "™"
	["\xc5\xa1"] = "\x9a", // "š"
	["\xe2\x80\xba"] = "\x9b", // "›"
	["\xc5\x9b"] = "\x9c", // "ś"
	["\xc5\xa5"] = "\x9d", // "ť"
	["\xc5\xbe"] = "\x9e", // "ž"
	["\xc5\xba"] = "\x9f", // "ź"

	["\x20"] = "\xa0", // NBSP
	["\xcb\x87"] = "\xa1", // "ˇ"
	["\xcb\x98"] = "\xa2", // "˘"
	["\xc5\x81"] = "\xa3", // "Ł"
	["\xc2\xa4"] = "\xa4", // "¤"
	["\xc4\x84"] = "\xa5", // "Ą"
	["\xc2\xa6"] = "\xa6", // "¦"
	["\xc2\xa7"] = "\xa7", // "§"
	["\xc2\xa8"] = "\xa8", // "¨"
	["\xc2\xa9"] = "\xa9", // "©"
	["\xc5\x9e"] = "\xaa", // "Ş"
	["\xc2\xab"] = "\xab", // "«"
	["\xc2\xac"] = "\xac", // "¬"
	["\xc2\xad"] = "\xad", // SHY
	["\xc2\xae"] = "\xae", // "®"
	["\xc5\xbb"] = "\xaf", // "Ż"

	["\xc2\xb0"] = "\xb0", // "°"
	["\xc2\xb1"] = "\xb1", // "±"
	["\xcb\x9b"] = "\xb2", // "˛"
	["\xc5\x82"] = "\xb3", // "ł"
	["\xc2\xb4"] = "\xb4", // "´"
	["\xc2\xb5"] = "\xb5", // "µ"
	["\xc2\xb6"] = "\xb6", // "¶"
	["\xc2\xb7"] = "\xb7", // "·"
	["\xc2\xb8"] = "\xb8", // "¸"
	["\xc4\x85"] = "\xb9", // "ą"
	["\xc5\x9f"] = "\xba", // "ş"
	["\xc2\xbb"] = "\xbb", // "»"
	["\xc4\xbd"] = "\xbc", // "Ľ"
	["\xcb\x9d"] = "\xbd", // "˝"
	["\xc4\xbe"] = "\xbe", // "ľ"
	["\xc5\xbc"] = "\xbf", // "ż"

	["\xc5\x94"] = "\xc0", // "Ŕ"
	["\xc3\x81"] = "\xc1", // "Á"
	["\xc3\x82"] = "\xc2", // "Â"
	["\xc4\x82"] = "\xc3", // "Ă"
	["\xc3\x84"] = "\xc4", // "Ä"
	["\xc4\xb9"] = "\xc5", // "Ĺ"
	["\xc4\x86"] = "\xc6", // "Ć"
	["\xc3\x87"] = "\xc7", // "Ç"
	["\xc4\x8c"] = "\xc8", // "Č"
	["\xc3\x89"] = "\xc9", // "É"
	["\xc4\x98"] = "\xca", // "Ę"
	["\xc3\x8b"] = "\xcb", // "Ë"
	["\xc4\x9a"] = "\xcc", // "Ě"
	["\xc3\x8d"] = "\xcd", // "Í"
	["\xc3\x8e"] = "\xce", // "Î"
	["\xc4\x8e"] = "\xcf", // "Ď"

	["\xc4\x90"] = "\xd0", // "Đ"
	["\xc5\x83"] = "\xd1", // "Ń"
	["\xc5\x87"] = "\xd2", // "Ň"
	["\xc3\x93"] = "\xd3", // "Ó"
	["\xc3\x94"] = "\xd4", // "Ô"
	["\xc5\x90"] = "\xd5", // "Ő"
	["\xc3\x96"] = "\xd6", // "Ö"
	["\xc3\x97"] = "\xd7", // "×"
	["\xc5\x98"] = "\xd8", // "Ř"
	["\xc5\xae"] = "\xd9", // "Ů"
	["\xc3\x9a"] = "\xda", // "Ú"
	["\xc5\xb0"] = "\xdb", // "Ű"
	["\xc3\x9c"] = "\xdc", // "Ü"
	["\xc3\x9d"] = "\xdd", // "Ý"
	["\xc5\xa2"] = "\xde", // "Ţ"
	["\xc3\x9f"] = "\xdf", // "ß"

	["\xc5\x95"] = "\xe0", // "ŕ"
	["\xc3\xa1"] = "\xe1", // "á"
	["\xc3\xa2"] = "\xe2", // "â"
	["\xc4\x83"] = "\xe3", // "ă"
	["\xc3\xa4"] = "\xe4", // "ä"
	["\xc4\xba"] = "\xe5", // "ĺ"
	["\xc4\x87"] = "\xe6", // "ć"
	["\xc3\xa7"] = "\xe7", // "ç"
	["\xc4\x8d"] = "\xe8", // "č"
	["\xc3\xa9"] = "\xe9", // "é"
	["\xc4\x99"] = "\xea", // "ę"
	["\xc3\xab"] = "\xeb", // "ë"
	["\xc4\x9b"] = "\xec", // "ě"
	["\xc3\xad"] = "\xed", // "í"
	["\xc3\xae"] = "\xee", // "î"
	["\xc4\x8f"] = "\xef", // "ď"

	["\xc4\x91"] = "\xf0", // "đ"
	["\xc5\x84"] = "\xf1", // "ń"
	["\xc5\x88"] = "\xf2", // "ň"
	["\xc3\xb3"] = "\xf3", // "ó"
	["\xc3\xb4"] = "\xf4", // "ô"
	["\xc5\x91"] = "\xf5", // "ő"
	["\xc3\xb6"] = "\xf6", // "ö"
	["\xc3\xb7"] = "\xf7", // "÷"
	["\xc5\x99"] = "\xf8", // "ř"
	["\xc5\xaf"] = "\xf9", // "ů"
	["\xc3\xba"] = "\xfa", // "ú"
	["\xc5\xb1"] = "\xfb", // "ű"
	["\xc3\xbc"] = "\xfc", // "ü"
	["\xc3\xbd"] = "\xfd", // "ý"
	["\xc5\xa3"] = "\xfe", // "ţ"
	["\xcb\x99"] = "\xff", // "˙"
}

function utf8_to_windows1250(text)
{
	local result = ""

	for (local i = 0, charSize = 0, textLen = text.len(); i != textLen; i += charSize)
	{
		local char = text[i]

		if (char < 0)
			char += 256

		if(char < 128)
			charSize = 1
		else
		{
			if(char < 224)
				charSize = 2
			else if(char < 240)
				charSize = 3
			else if(char < 248)
				charSize = 4
			else if(char == 252)
			charSize = 5
			else
				charSize = 6
		}

		local charBytes = text.slice(i, i + charSize)

		if (charSize == 1)
			result += charBytes
		else if (charBytes in utf8_windows1250_map)
			result += utf8_windows1250_map[charBytes]
		else
			result += "?"
	}

	return result
}