local utf8_windows1253_map =
{
	["\xe2\x82\xac"] = "\x80", // "€"
	["\xe2\x80\x9a"] = "\x82", // "‚"
	["\xc6\x92"] = "\x83", // "ƒ"
	["\xe2\x80\x9e"] = "\x84", // "„"
	["\xe2\x80\xa6"] = "\x85", // "…"
	["\xe2\x80\xa0"] = "\x86", // "†"
	["\xe2\x80\xa1"] = "\x87", // "‡"
	["\xe2\x80\xb0"] = "\x89", // "‰"
	["\xe2\x80\xb9"] = "\x8b", // "‹"

	["\xe2\x80\x98"] = "\x91", // "‘"
	["\xe2\x80\x99"] = "\x92", // "’"
	["\xe2\x80\x9c"] = "\x93", // "“"
	["\xe2\x80\x9d"] = "\x94", // "”"
	["\xe2\x80\xa2"] = "\x95", // "•"
	["\xe2\x80\x93"] = "\x96", // "–"
	["\xe2\x80\x94"] = "\x97", // "—"
	["\xe2\x84\xa2"] = "\x99", // "™"
	["\xe2\x80\xba"] = "\x9b", // "›"

	["\x20"] = "\xa0", // NBSP
	["\xce\x85"] = "\xa1", // "΅"
	["\xce\x86"] = "\xa2", // "Ά"
	["\xc2\xa3"] = "\xa3", // "£"
	["\xc2\xa4"] = "\xa4", // "¤"
	["\xc2\xa5"] = "\xa5", // "¥"
	["\xc2\xa6"] = "\xa6", // "¦"
	["\xc2\xa7"] = "\xa7", // "§"
	["\xc2\xa8"] = "\xa8", // "¨"
	["\xc2\xa9"] = "\xa9", // "©"
	["\xc2\xab"] = "\xab", // "«"
	["\xc2\xac"] = "\xac", // "¬"
	["\xc2\xad"] = "\xad", // "SHY"
	["\xc2\xae"] = "\xae", // "®"
	["\xe2\x80\x95"] = "\xaf", // "―"

	["\xc2\xb0"] = "\xb0", // "°"
	["\xc2\xb1"] = "\xb1", // "±"
	["\xc2\xb2"] = "\xb2", // "²"
	["\xc2\xb3"] = "\xb3", // "³"
	["\xce\x84"] = "\xb4", // "΄"
	["\xc2\xb5"] = "\xb5", // "µ"
	["\xc2\xb6"] = "\xb6", // "¶"
	["\xc2\xb7"] = "\xb7", // "·"
	["\xce\x88"] = "\xb8", // "Έ"
	["\xce\x89"] = "\xb9", // "Ή"
	["\xce\x8a"] = "\xba", // "Ί"
	["\xc2\xbb"] = "\xbb", // "»"
	["\xce\x8c"] = "\xbc", // "Ό"
	["\xc2\xbd"] = "\xbd", // "½"
	["\xce\x8e"] = "\xbe", // "Ύ"
	["\xce\x8f"] = "\xbf", // "Ώ"

	["\xce\x90"] = "\xc0", // "ΐ"
	["\xce\x91"] = "\xc1", // "Α"
	["\xce\x92"] = "\xc2", // "Β"
	["\xce\x93"] = "\xc3", // "Γ"
	["\xce\x94"] = "\xc4", // "Δ"
	["\xce\x95"] = "\xc5", // "Ε"
	["\xce\x96"] = "\xc6", // "Ζ"
	["\xce\x97"] = "\xc7", // "Η"
	["\xce\x98"] = "\xc8", // "Θ"
	["\xce\x99"] = "\xc9", // "Ι"
	["\xce\x9a"] = "\xca", // "Κ"
	["\xce\x9b"] = "\xcb", // "Λ"
	["\xce\x9c"] = "\xcc", // "Μ"
	["\xce\x9d"] = "\xcd", // "Ν"
	["\xce\x9e"] = "\xce", // "Ξ"
	["\xce\x9f"] = "\xcf", // "Ο"

	["\xce\xa0"] = "\xd0", // "Π"
	["\xce\xa1"] = "\xd1", // "Ρ"
	["\xce\xa3"] = "\xd3", // "Σ"
	["\xce\xa4"] = "\xd4", // "Τ"
	["\xce\xa5"] = "\xd5", // "Υ"
	["\xce\xa6"] = "\xd6", // "Φ"
	["\xce\xa7"] = "\xd7", // "Χ"
	["\xce\xa8"] = "\xd8", // "Ψ"
	["\xce\xa9"] = "\xd9", // "Ω"
	["\xce\xaa"] = "\xda", // "Ϊ"
	["\xce\xab"] = "\xdb", // "Ϋ"
	["\xce\xac"] = "\xdc", // "ά"
	["\xce\xad"] = "\xdd", // "έ"
	["\xce\xae"] = "\xde", // "ή"
	["\xce\xaf"] = "\xdf", // "ί"

	["\xce\xb0"] = "\xe0", // "ΰ"
	["\xce\xb1"] = "\xe1", // "α"
	["\xce\xb2"] = "\xe2", // "β"
	["\xce\xb3"] = "\xe3", // "γ"
	["\xce\xb4"] = "\xe4", // "δ"
	["\xce\xb5"] = "\xe5", // "ε"
	["\xce\xb6"] = "\xe6", // "ζ"
	["\xce\xb7"] = "\xe7", // "η"
	["\xce\xb8"] = "\xe8", // "θ"
	["\xce\xb9"] = "\xe9", // "ι"
	["\xce\xba"] = "\xea", // "κ"
	["\xce\xbb"] = "\xeb", // "λ"
	["\xce\xbc"] = "\xec", // "μ"
	["\xce\xbd"] = "\xed", // "ν"
	["\xce\xbe"] = "\xee", // "ξ"
	["\xce\xbf"] = "\xef", // "ο"

	["\xcf\x80"] = "\xf0", // "π"
	["\xcf\x81"] = "\xf1", // "ρ"
	["\xcf\x82"] = "\xf2", // "ς"
	["\xcf\x83"] = "\xf3", // "σ"
	["\xcf\x84"] = "\xf4", // "τ"
	["\xcf\x85"] = "\xf5", // "υ"
	["\xcf\x86"] = "\xf6", // "φ"
	["\xcf\x87"] = "\xf7", // "χ"
	["\xcf\x88"] = "\xf8", // "ψ"
	["\xcf\x89"] = "\xf9", // "ω"
	["\xcf\x8a"] = "\xfa", // "ϊ"
	["\xcf\x8b"] = "\xfb", // "ϋ"
	["\xcf\x8c"] = "\xfc", // "ό"
	["\xcf\x8d"] = "\xfd", // "ύ"
	["\xcf\x8e"] = "\xfe", // "ώ"
}

function utf8_to_windows1253(text)
{
	local result = ""

	for (local i = 0, charSize = 0, textLen = text.len(); i != textLen; i += charSize)
	{
		local char = text[i]

		if (char < 0)
			char += 256

		if(char < 128)
			charSize = 1
		else
		{
			if(char < 224)
				charSize = 2
			else if(char < 240)
				charSize = 3
			else if(char < 248)
				charSize = 4
			else if(char == 252)
			charSize = 5
			else
				charSize = 6
		}

		local charBytes = text.slice(i, i + charSize)

		if (charSize == 1)
			result += charBytes
		else if (charBytes in utf8_windows1253_map)
			result += utf8_windows1253_map[charBytes]
		else
			result += "?"
	}

	return result
}