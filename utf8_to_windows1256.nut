local utf8_windows1256_map =
{
	["\xe2\x82\xac"] = "\x80", // "€"
	["\xd9\xbe"] = "\x81", // "پ"
	["\xe2\x80\x9a"] = "\x82", // "‚"
	["\xc6\x92"] = "\x83", // "ƒ"
	["\xe2\x80\x9e"] = "\x84", // "„"
	["\xe2\x80\xa6"] = "\x85", // "…"
	["\xe2\x80\xa0"] = "\x86", // "†"
	["\xe2\x80\xa1"] = "\x87", // "‡"
	["\xcb\x86"] = "\x88", // "ˆ"
	["\xe2\x80\xb0"] = "\x89", // "‰"
	["\xd9\xb9"] = "\x8a", // "ٹ"
	["\xe2\x80\xb9"] = "\x8b", // "‹"
	["\xc5\x92"] = "\x8c", // "Œ"
	["\xda\x86"] = "\x8d", // "چ"
	["\xda\x98"] = "\x8e", // "ژ"
	["\xda\x88"] = "\x8f", // "ڈ"

	["\xda\xaf"] = "\x90", // "گ"
	["\xe2\x80\x98"] = "\x91", // "‘"
	["\xe2\x80\x99"] = "\x92", // "’"
	["\xe2\x80\x9c"] = "\x93", // "“"
	["\xe2\x80\x9d"] = "\x94", // "”"
	["\xe2\x80\xa2"] = "\x95", // "•"
	["\xe2\x80\x93"] = "\x96", // "–"
	["\xe2\x80\x94"] = "\x97", // "—"
	["\xda\xa9"] = "\x98", // "ک"
	["\xe2\x84\xa2"] = "\x99", // "™"
	["\xda\x91"] = "\x9a", // "ڑ"
	["\xe2\x80\xba"] = "\x9b", // "›"
	["\xc5\x93"] = "\x9c", // "œ"
	["\xe2\x80\x8c"] = "\x9d", // ZWNJ
	["\xe2\x80\x8d"] = "\x9e", // ZWJ
	["\xda\xba"] = "\x9f", // "ں"

	["\x20"] = "\xa0", // NBSP
	["\xd8\x8c"] = "\xa1", // "،"
	["\xc2\xa2"] = "\xa2", // "¢"
	["\xc2\xa3"] = "\xa3", // "£"
	["\xc2\xa4"] = "\xa4", // "¤"
	["\xc2\xa5"] = "\xa5", // "¥"
	["\xc2\xa6"] = "\xa6", // "¦"
	["\xc2\xa7"] = "\xa7", // "§"
	["\xc2\xa8"] = "\xa8", // "¨"
	["\xc2\xa9"] = "\xa9", // "©"
	["\xda\xbe"] = "\xaa", // "ھ"
	["\xc2\xab"] = "\xab", // "«"
	["\xc2\xac"] = "\xac", // "¬"
	["\xc2\xad"] = "\xad", // SHY
	["\xc2\xae"] = "\xae", // "®"
	["\xc2\xaf"] = "\xaf", // "¯"

	["\xc2\xb0"] = "\xb0", // "°"
	["\xc2\xb1"] = "\xb1", // "±"
	["\xc2\xb2"] = "\xb2", // "²"
	["\xc2\xb3"] = "\xb3", // "³"
	["\xc2\xb4"] = "\xb4", // "´"
	["\xc2\xb5"] = "\xb5", // "µ"
	["\xc2\xb6"] = "\xb6", // "¶"
	["\xc2\xb7"] = "\xb7", // "·"
	["\xc2\xb8"] = "\xb8", // "¸"
	["\xc2\xb9"] = "\xb9", // "¹"
	["\xd8\x9b"] = "\xba", // "؛"
	["\xc2\xbb"] = "\xbb", // "»"
	["\xc2\xbc"] = "\xbc", // "¼"
	["\xc2\xbd"] = "\xbd", // "½"
	["\xc2\xbe"] = "\xbe", // "¾"
	["\xd8\x9f"] = "\xbf", // "؟"

	["\xdb\x81"] = "\xc0", // "ہ"
	["\xd8\xa1"] = "\xc1", // "ء"
	["\xd8\xa2"] = "\xc2", // "آ"
	["\xd8\xa3"] = "\xc3", // "أ"
	["\xd8\xa4"] = "\xc4", // "ؤ"
	["\xd8\xa5"] = "\xc5", // "إ"
	["\xd8\xa6"] = "\xc6", // "ئ"
	["\xd8\xa7"] = "\xc7", // "ا"
	["\xd8\xa8"] = "\xc8", // "ب"
	["\xd8\xa9"] = "\xc9", // "ة"
	["\xd8\xaa"] = "\xca", // "ت"
	["\xd8\xab"] = "\xcb", // "ث"
	["\xd8\xac"] = "\xcc", // "ج"
	["\xd8\xad"] = "\xcd", // "ح"
	["\xd8\xae"] = "\xce", // "خ"
	["\xd8\xaf"] = "\xcf", // "د"

	["\xd8\xb0"] = "\xd0", // "ذ"
	["\xd8\xb1"] = "\xd1", // "ر"
	["\xd8\xb2"] = "\xd2", // "ز"
	["\xd8\xb3"] = "\xd3", // "س"
	["\xd8\xb4"] = "\xd4", // "ش"
	["\xd8\xb5"] = "\xd5", // "ص"
	["\xd8\xb6"] = "\xd6", // "ض"
	["\xc3\x97"] = "\xd7", // "×"
	["\xd8\xb7"] = "\xd8", // "ط"
	["\xd8\xb8"] = "\xd9", // "ظ"
	["\xd8\xb9"] = "\xda", // "ع"
	["\xd8\xba"] = "\xdb", // "غ"
	["\xd9\x80"] = "\xdc", // "ـ"
	["\xd9\x81"] = "\xdd", // "ف"
	["\xd9\x82"] = "\xde", // "ق"
	["\xd9\x83"] = "\xdf", // "ك"

	["\xc3\xa0"] = "\xe0", // "à"
	["\xd9\x84"] = "\xe1", // "ل"
	["\xc3\xa2"] = "\xe2", // "â"
	["\xd9\x85"] = "\xe3", // "م"
	["\xd9\x86"] = "\xe4", // "ن"
	["\xd9\x87"] = "\xe5", // "ه"
	["\xd9\x88"] = "\xe6", // "و"
	["\xc3\xa7"] = "\xe7", // "ç"
	["\xc3\xa8"] = "\xe8", // "è"
	["\xc3\xa9"] = "\xe9", // "é"
	["\xc3\xaa"] = "\xea", // "ê"
	["\xc3\xab"] = "\xeb", // "ë"
	["\xd9\x89"] = "\xec", // "ى"
	["\xd9\x8a"] = "\xed", // "ي"
	["\xc3\xae"] = "\xee", // "î"
	["\xc3\xaf"] = "\xef", // "ï"

	["\xd9\x8b"] = "\xf0", // "ً"
	["\xd9\x8c"] = "\xf1", // "ٌ"
	["\xd9\x8d"] = "\xf2", // "ٍ"
	["\xd9\x8e"] = "\xf3", // "َ"
	["\xc3\xb4"] = "\xf4", // "ô"
	["\xd9\x8f"] = "\xf5", // "ُ"
	["\xd9\x90"] = "\xf6", // "ِ"
	["\xc3\xb7"] = "\xf7", // "÷"
	["\xd9\x91"] = "\xf8", // "ّ"
	["\xc3\xb9"] = "\xf9", // "ù"
	["\xd9\x92"] = "\xfa", // "ْ"
	["\xc3\xbb"] = "\xfb", // "û"
	["\xc3\xbc"] = "\xfc", // "ü"
	["\xe2\x80\x8e"] = "\xfd", // LRM
	["\xe2\x80\x8f"] = "\xfe", // RLM
	["\xdb\x92"] = "\xff", // "ے"
}

function utf8_to_windows1256(text)
{
	local result = ""

	for (local i = 0, charSize = 0, textLen = text.len(); i != textLen; i += charSize)
	{
		local char = text[i]

		if (char < 0)
			char += 256

		if(char < 128)
			charSize = 1
		else
		{
			if(char < 224)
				charSize = 2
			else if(char < 240)
				charSize = 3
			else if(char < 248)
				charSize = 4
			else if(char == 252)
			charSize = 5
			else
				charSize = 6
		}

		local charBytes = text.slice(i, i + charSize)

		if (charSize == 1)
			result += charBytes
		else if (charBytes in utf8_windows1256_map)
			result += utf8_windows1256_map[charBytes]
		else
			result += "?"
	}

	return result
}