local windows1253_utf8_map =
{
	[0x80] = "\xe2\x82\xac", // "€"
	[0x82] = "\xe2\x80\x9a", // "‚"
	[0x83] = "\xc6\x92", // "ƒ"
	[0x84] = "\xe2\x80\x9e", // "„"
	[0x85] = "\xe2\x80\xa6", // "…"
	[0x86] = "\xe2\x80\xa0", // "†"
	[0x87] = "\xe2\x80\xa1", // "‡"
	[0x89] = "\xe2\x80\xb0", // "‰"
	[0x8b] = "\xe2\x80\xb9", // "‹"

	[0x91] = "\xe2\x80\x98", // "‘"
	[0x92] = "\xe2\x80\x99", // "’"
	[0x93] = "\xe2\x80\x9c", // "“"
	[0x94] = "\xe2\x80\x9d", // "”"
	[0x95] = "\xe2\x80\xa2", // "•"
	[0x96] = "\xe2\x80\x93", // "–"
	[0x97] = "\xe2\x80\x94", // "—"
	[0x99] = "\xe2\x84\xa2", // "™"
	[0x9b] = "\xe2\x80\xba", // "›"

	[0xa0] = "\x20", // NBSP
	[0xa1] = "\xce\x85", // "΅"
	[0xa2] = "\xce\x86", // "Ά"
	[0xa3] = "\xc2\xa3", // "£"
	[0xa4] = "\xc2\xa4", // "¤"
	[0xa5] = "\xc2\xa5", // "¥"
	[0xa6] = "\xc2\xa6", // "¦"
	[0xa7] = "\xc2\xa7", // "§"
	[0xa8] = "\xc2\xa8", // "¨"
	[0xa9] = "\xc2\xa9", // "©"
	[0xab] = "\xc2\xab", // "«"
	[0xac] = "\xc2\xac", // "¬"
	[0xad] = "\xc2\xad", // SHY
	[0xae] = "\xc2\xae", // "®"
	[0xaf] = "\xe2\x80\x95", // "―"

	[0xb0] = "\xc2\xb0", // "°"
	[0xb1] = "\xc2\xb1", // "±"
	[0xb2] = "\xc2\xb2", // "²"
	[0xb3] = "\xc2\xb3", // "³"
	[0xb4] = "\xce\x84", // "΄"
	[0xb5] = "\xc2\xb5", // "µ"
	[0xb6] = "\xc2\xb6", // "¶"
	[0xb7] = "\xc2\xb7", // "·"
	[0xb8] = "\xce\x88", // "Έ"
	[0xb9] = "\xce\x89", // "Ή"
	[0xba] = "\xce\x8a", // "Ί"
	[0xbb] = "\xc2\xbb", // "»"
	[0xbc] = "\xce\x8c", // "Ό"
	[0xbd] = "\xc2\xbd", // "½"
	[0xbe] = "\xce\x8e", // "Ύ"
	[0xbf] = "\xce\x8f", // "Ώ"

	[0xc0] = "\xce\x90", // "ΐ"
	[0xc1] = "\xce\x91", // "Α"
	[0xc2] = "\xce\x92", // "Β"
	[0xc3] = "\xce\x93", // "Γ"
	[0xc4] = "\xce\x94", // "Δ"
	[0xc5] = "\xce\x95", // "Ε"
	[0xc6] = "\xce\x96", // "Ζ"
	[0xc7] = "\xce\x97", // "Η"
	[0xc8] = "\xce\x98", // "Θ"
	[0xc9] = "\xce\x99", // "Ι"
	[0xca] = "\xce\x9a", // "Κ"
	[0xcb] = "\xce\x9b", // "Λ"
	[0xcc] = "\xce\x9c", // "Μ"
	[0xcd] = "\xce\x9d", // "Ν"
	[0xce] = "\xce\x9e", // "Ξ"
	[0xcf] = "\xce\x9f", // "Ο"

	[0xd0] = "\xce\xa0", // "Π"
	[0xd1] = "\xce\xa1", // "Ρ"
	[0xd3] = "\xce\xa3", // "Σ"
	[0xd4] = "\xce\xa4", // "Τ"
	[0xd5] = "\xce\xa5", // "Υ"
	[0xd6] = "\xce\xa6", // "Φ"
	[0xd7] = "\xce\xa7", // "Χ"
	[0xd8] = "\xce\xa8", // "Ψ"
	[0xd9] = "\xce\xa9", // "Ω"
	[0xda] = "\xce\xaa", // "Ϊ"
	[0xdb] = "\xce\xab", // "Ϋ"
	[0xdc] = "\xce\xac", // "ά"
	[0xdd] = "\xce\xad", // "έ"
	[0xde] = "\xce\xae", // "ή"
	[0xdf] = "\xce\xaf", // "ί"

	[0xe0] = "\xce\xb0", // "ΰ"
	[0xe1] = "\xce\xb1", // "α"
	[0xe2] = "\xce\xb2", // "β"
	[0xe3] = "\xce\xb3", // "γ"
	[0xe4] = "\xce\xb4", // "δ"
	[0xe5] = "\xce\xb5", // "ε"
	[0xe6] = "\xce\xb6", // "ζ"
	[0xe7] = "\xce\xb7", // "η"
	[0xe8] = "\xce\xb8", // "θ"
	[0xe9] = "\xce\xb9", // "ι"
	[0xea] = "\xce\xba", // "κ"
	[0xeb] = "\xce\xbb", // "λ"
	[0xec] = "\xce\xbc", // "μ"
	[0xed] = "\xce\xbd", // "ν"
	[0xee] = "\xce\xbe", // "ξ"
	[0xef] = "\xce\xbf", // "ο"

	[0xf0] = "\xcf\x80", // "π"
	[0xf1] = "\xcf\x81", // "ρ"
	[0xf2] = "\xcf\x82", // "ς"
	[0xf3] = "\xcf\x83", // "σ"
	[0xf4] = "\xcf\x84", // "τ"
	[0xf5] = "\xcf\x85", // "υ"
	[0xf6] = "\xcf\x86", // "φ"
	[0xf7] = "\xcf\x87", // "χ"
	[0xf8] = "\xcf\x88", // "ψ"
	[0xf9] = "\xcf\x89", // "ω"
	[0xfa] = "\xcf\x8a", // "ϊ"
	[0xfb] = "\xcf\x8b", // "ϋ"
	[0xfc] = "\xcf\x8c", // "ό"
	[0xfd] = "\xcf\x8d", // "ύ"
	[0xfe] = "\xcf\x8e", // "ώ"
}

function windows1253_to_utf8(text)
{
	local result = ""

	foreach (idx, charId in text)
	{
		charId += 256

		if (charId in windows1253_utf8_map)
			result += windows1253_utf8_map[charId]
		else
			result += text.slice(idx, idx + 1)
	}

	return result
}