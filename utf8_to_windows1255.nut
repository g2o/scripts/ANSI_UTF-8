local utf8_windows1255_map =
{
	["\xe2\x82\xac"] = "\x80", // "€"
	["\xe2\x80\x9a"] = "\x82", // "‚"
	["\xc6\x92"] = "\x83", // "ƒ"
	["\xe2\x80\x9e"] = "\x84", // "„"
	["\xe2\x80\xa6"] = "\x85", // "…"
	["\xe2\x80\xa0"] = "\x86", // "†"
	["\xe2\x80\xa1"] = "\x87", // "‡"
	["\xcb\x86"] = "\x88", // "ˆ"
	["\xe2\x80\xb0"] = "\x89", // "‰"
	["\xe2\x80\xb9"] = "\x8b", // "‹"

	["\xe2\x80\x98"] = "\x91", // "‘"
	["\xe2\x80\x99"] = "\x92", // "’"
	["\xe2\x80\x9c"] = "\x93", // "“"
	["\xe2\x80\x9d"] = "\x94", // "”"
	["\xe2\x80\xa2"] = "\x95", // "•"
	["\xe2\x80\x93"] = "\x96", // "–"
	["\xe2\x80\x94"] = "\x97", // "—"
	["\xcb\x9c"] = "\x98", // "˜"
	["\xe2\x84\xa2"] = "\x99", // "™"
	["\xe2\x80\xba"] = "\x9b", // "›"

	["\x20"] = "\xa0", // NBSP
	["\xc2\xa1"] = "\xa1", // "¡"
	["\xc2\xa2"] = "\xa2", // "¢"
	["\xc2\xa3"] = "\xa3", // "£"
	["\xe2\x82\xaa"] = "\xa4", // "₪"
	["\xc2\xa5"] = "\xa5", // "¥"
	["\xc2\xa6"] = "\xa6", // "¦"
	["\xc2\xa7"] = "\xa7", // "§"
	["\xc2\xa8"] = "\xa8", // "¨"
	["\xc2\xa9"] = "\xa9", // "©"
	["\xc3\x97"] = "\xaa", // "×"
	["\xc2\xab"] = "\xab", // "«"
	["\xc2\xac"] = "\xac", // "¬"
	["\xc2\xad"] = "\xad", // SHY
	["\xc2\xae"] = "\xae", // "®"
	["\xc2\xaf"] = "\xaf", // "¯"

	["\xc2\xb0"] = "\xb0", // "°"
	["\xc2\xb1"] = "\xb1", // "±"
	["\xc2\xb2"] = "\xb2", // "²"
	["\xc2\xb3"] = "\xb3", // "³"
	["\xc2\xb4"] = "\xb4", // "´"
	["\xc2\xb5"] = "\xb5", // "µ"
	["\xc2\xb6"] = "\xb6", // "¶"
	["\xc2\xb7"] = "\xb7", // "·"
	["\xc2\xb8"] = "\xb8", // "¸"
	["\xc2\xb9"] = "\xb9", // "¹"
	["\xc3\xb7"] = "\xba", // "÷"
	["\xc2\xbb"] = "\xbb", // "»"
	["\xc2\xbc"] = "\xbc", // "¼"
	["\xc2\xbd"] = "\xbd", // "½"
	["\xc2\xbe"] = "\xbe", // "¾"
	["\xc2\xbf"] = "\xbf", // "¿"

	["\xd6\xb0"] = "\xc0", // "ְ"
	["\xd6\xb1"] = "\xc1", // "ֱ"
	["\xd6\xb2"] = "\xc2", // "ֲ"
	["\xd6\xb3"] = "\xc3", // "ֳ"
	["\xd6\xb4"] = "\xc4", // "ִ"
	["\xd6\xb5"] = "\xc5", // "ֵ"
	["\xd6\xb6"] = "\xc6", // "ֶ"
	["\xd6\xb7"] = "\xc7", // "ַ"
	["\xd6\xb8"] = "\xc8", // "ָ"
	["\x20\xd6\xb9"] = "\xc9", // " ֹ"
	["\x20\xd6\xba"] = "\xca", // " ֺ"
	["\xd6\xbb"] = "\xcb", // "ֻ"
	["\xd6\xbc"] = "\xcc", // "ּ"
	["\xd6\xbd"] = "\xcd", // "ֽ"
	["\xd6\xbe"] = "\xce", // "־"
	["\xd6\xbf"] = "\xcf", // "ֿ"

	["\xd7\x80"] = "\xd0", // "׀"
	["\xd7\x81"] = "\xd1", // "ׁ"
	["\xd7\x82"] = "\xd2", // "ׂ"
	["\xd7\x83"] = "\xd3", // "׃"
	["\xd7\xb0"] = "\xd4", // "װ"
	["\xd7\xb1"] = "\xd5", // "ױ"
	["\xd7\xb2"] = "\xd6", // "ײ"
	["\xd7\xb3"] = "\xd7", // "׳"
	["\xd7\xb4"] = "\xd8", // "״"

	["\xd7\x90"] = "\xe0", // "א"
	["\xd7\x91"] = "\xe1", // "ב"
	["\xd7\x92"] = "\xe2", // "ג"
	["\xd7\x93"] = "\xe3", // "ד"
	["\xd7\x94"] = "\xe4", // "ה"
	["\xd7\x95"] = "\xe5", // "ו"
	["\xd7\x96"] = "\xe6", // "ז"
	["\xd7\x97"] = "\xe7", // "ח"
	["\xd7\x98"] = "\xe8", // "ט"
	["\xd7\x99"] = "\xe9", // "י"
	["\xd7\x9a"] = "\xea", // "ך"
	["\xd7\x9b"] = "\xeb", // "כ"
	["\xd7\x9c"] = "\xec", // "ל"
	["\xd7\x9d"] = "\xed", // "ם"
	["\xd7\x9e"] = "\xee", // "מ"
	["\xd7\x9f"] = "\xef", // "ן"

	["\xd7\xa0"] = "\xf0", // "נ"
	["\xd7\xa1"] = "\xf1", // "ס"
	["\xd7\xa2"] = "\xf2", // "ע"
	["\xd7\xa3"] = "\xf3", // "ף"
	["\xd7\xa4"] = "\xf4", // "פ"
	["\xd7\xa5"] = "\xf5", // "ץ"
	["\xd7\xa6"] = "\xf6", // "צ"
	["\xd7\xa7"] = "\xf7", // "ק"
	["\xd7\xa8"] = "\xf8", // "ר"
	["\xd7\xa9"] = "\xf9", // "ש"
	["\xd7\xaa"] = "\xfa", // "ת"
	["\x4c\x52\x4d"] = "\xfd", // LRM
	["\x52\x4c\x4d"] = "\xfe", // RLM
}

function utf8_to_windows1255(text)
{
	local result = ""

	for (local i = 0, charSize = 0, textLen = text.len(); i != textLen; i += charSize)
	{
		local char = text[i]

		if (char < 0)
			char += 256

		if(char < 128)
			charSize = 1
		else
		{
			if(char < 224)
				charSize = 2
			else if(char < 240)
				charSize = 3
			else if(char < 248)
				charSize = 4
			else if(char == 252)
			charSize = 5
			else
				charSize = 6
		}

		local charBytes = text.slice(i, i + charSize)

		if (charSize == 1)
			result += charBytes
		else if (charBytes in utf8_windows1255_map)
			result += utf8_windows1255_map[charBytes]
		else
			result += "?"
	}

	return result
}