local windows1257_utf8_map =
{
	[0x80] = "\xe2\x82\xac", // "€"
	[0x82] = "\xe2\x80\x9a", // "‚"
	[0x84] = "\xe2\x80\x9e", // "„"
	[0x85] = "\xe2\x80\xa6", // "…"
	[0x86] = "\xe2\x80\xa0", // "†"
	[0x87] = "\xe2\x80\xa1", // "‡"
	[0x89] = "\xe2\x80\xb0", // "‰"
	[0x8b] = "\xe2\x80\xb9", // "‹"
	[0x8d] = "\xc2\xa8", // "¨"
	[0x8e] = "\xcb\x87", // "ˇ"
	[0x8f] = "\xc2\xb8", // "¸"

	[0x91] = "\xe2\x80\x98", // "‘"
	[0x92] = "\xe2\x80\x99", // "’"
	[0x93] = "\xe2\x80\x9c", // "“"
	[0x94] = "\xe2\x80\x9d", // "”"
	[0x95] = "\xe2\x80\xa2", // "•"
	[0x96] = "\xe2\x80\x93", // "–"
	[0x97] = "\xe2\x80\x94", // "—"
	[0x99] = "\xe2\x84\xa2", // "™"
	[0x9b] = "\xe2\x80\xba", // "›"
	[0x9d] = "\xc2\xaf", // "¯"
	[0x9e] = "\xcb\x9b", // "˛"

	[0xa0] = "\x20", // NBSP
	[0xa2] = "\xc2\xa2", // "¢"
	[0xa3] = "\xc2\xa3", // "£"
	[0xa4] = "\xc2\xa4", // "¤"
	[0xa6] = "\xc2\xa6", // "¦"
	[0xa7] = "\xc2\xa7", // "§"
	[0xa8] = "\xc3\x98", // "Ø"
	[0xa9] = "\xc2\xa9", // "©"
	[0xaa] = "\xc5\x96", // "Ŗ"
	[0xab] = "\xc2\xab", // "«"
	[0xac] = "\xc2\xac", // "¬"
	[0xad] = "\xc2\xad", // SHY
	[0xae] = "\xc2\xae", // "®"
	[0xaf] = "\xc3\x86", // "Æ"

	[0xb0] = "\xc2\xb0", // "°"
	[0xb1] = "\xc2\xb1", // "±"
	[0xb2] = "\xc2\xb2", // "²"
	[0xb3] = "\xc2\xb3", // "³"
	[0xb4] = "\xc2\xb4", // "´"
	[0xb5] = "\xc2\xb5", // "µ"
	[0xb6] = "\xc2\xb6", // "¶"
	[0xb7] = "\xc2\xb7", // "·"
	[0xb8] = "\xc3\xb8", // "ø"
	[0xb9] = "\xc2\xb9", // "¹"
	[0xba] = "\xc5\x97", // "ŗ"
	[0xbb] = "\xc2\xbb", // "»"
	[0xbc] = "\xc2\xbc", // "¼"
	[0xbd] = "\xc2\xbd", // "½"
	[0xbe] = "\xc2\xbe", // "¾"
	[0xbf] = "\xc3\xa6", // "æ"

	[0xc0] = "\xc4\x84", // "Ą"
	[0xc1] = "\xc4\xae", // "Į"
	[0xc2] = "\xc4\x80", // "Ā"
	[0xc3] = "\xc4\x86", // "Ć"
	[0xc4] = "\xc3\x84", // "Ä"
	[0xc5] = "\xc3\x85", // "Å"
	[0xc6] = "\xc4\x98", // "Ę"
	[0xc7] = "\xc4\x92", // "Ē"
	[0xc8] = "\xc4\x8c", // "Č"
	[0xc9] = "\xc3\x89", // "É"
	[0xca] = "\xc5\xb9", // "Ź"
	[0xcb] = "\xc4\x96", // "Ė"
	[0xcc] = "\xc4\xa2", // "Ģ"
	[0xcd] = "\xc4\xb6", // "Ķ"
	[0xce] = "\xc4\xaa", // "Ī"
	[0xcf] = "\xc4\xbb", // "Ļ"

	[0xd0] = "\xc5\xa0", // "Š"
	[0xd1] = "\xc5\x83", // "Ń"
	[0xd2] = "\xc5\x85", // "Ņ"
	[0xd3] = "\xc3\x93", // "Ó"
	[0xd4] = "\xc5\x8c", // "Ō"
	[0xd5] = "\xc3\x95", // "Õ"
	[0xd6] = "\xc3\x96", // "Ö"
	[0xd7] = "\xc3\x97", // "×"
	[0xd8] = "\xc5\xb2", // "Ų"
	[0xd9] = "\xc5\x81", // "Ł"
	[0xda] = "\xc5\x9a", // "Ś"
	[0xdb] = "\xc5\xaa", // "Ū"
	[0xdc] = "\xc3\x9c", // "Ü"
	[0xdd] = "\xc5\xbb", // "Ż"
	[0xde] = "\xc5\xbd", // "Ž"
	[0xdf] = "\xc3\x9f", // "ß"

	[0xe0] = "\xc4\x85", // "ą"
	[0xe1] = "\xc4\xaf", // "į"
	[0xe2] = "\xc4\x81", // "ā"
	[0xe3] = "\xc4\x87", // "ć"
	[0xe4] = "\xc3\xa4", // "ä"
	[0xe5] = "\xc3\xa5", // "å"
	[0xe6] = "\xc4\x99", // "ę"
	[0xe7] = "\xc4\x93", // "ē"
	[0xe8] = "\xc4\x8d", // "č"
	[0xe9] = "\xc3\xa9", // "é"
	[0xea] = "\xc5\xba", // "ź"
	[0xeb] = "\xc4\x97", // "ė"
	[0xec] = "\xc4\xa3", // "ģ"
	[0xed] = "\xc4\xb7", // "ķ"
	[0xee] = "\xc4\xab", // "ī"
	[0xef] = "\xc4\xbc", // "ļ"

	[0xf0] = "\xc5\xa1", // "š"
	[0xf1] = "\xc5\x84", // "ń"
	[0xf2] = "\xc5\x86", // "ņ"
	[0xf3] = "\xc3\xb3", // "ó"
	[0xf4] = "\xc5\x8d", // "ō"
	[0xf5] = "\xc3\xb5", // "õ"
	[0xf6] = "\xc3\xb6", // "ö"
	[0xf7] = "\xc3\xb7", // "÷"
	[0xf8] = "\xc5\xb3", // "ų"
	[0xf9] = "\xc5\x82", // "ł"
	[0xfa] = "\xc5\x9b", // "ś"
	[0xfb] = "\xc5\xab", // "ū"
	[0xfc] = "\xc3\xbc", // "ü"
	[0xfd] = "\xc5\xbc", // "ż"
	[0xfe] = "\xc5\xbe", // "ž"
	[0xff] = "\xcb\x99", // "˙"
}

function windows1257_to_utf8(text)
{
	local result = ""

	foreach (idx, charId in text)
	{
		charId += 256

		if (charId in windows1257_utf8_map)
			result += windows1257_utf8_map[charId]
		else
			result += text.slice(idx, idx + 1)
	}

	return result
}