local utf8_windows1251_map =
{
	["\xd0\x82"] = "\x80", // "Ђ"
	["\xd0\x83"] = "\x81", // "Ѓ"
	["\xe2\x80\x9a"] = "\x82", // "‚"
	["\xd1\x93"] = "\x83", // "ѓ"
	["\xe2\x80\x9e"] = "\x84", // "„"
	["\xe2\x80\xa6"] = "\x85", // "…"
	["\xe2\x80\xa0"] = "\x86", // "†"
	["\xe2\x80\xa1"] = "\x87", // "‡"
	["\xe2\x82\xac"] = "\x88", // "€"
	["\xe2\x80\xb0"] = "\x89", // "‰"
	["\xd0\x89"] = "\x8a", // "Љ"
	["\xe2\x80\xb9"] = "\x8b", // "‹"
	["\xd0\x8a"] = "\x8c", // "Њ"
	["\xd0\x8c"] = "\x8d", // "Ќ"
	["\xd0\x8b"] = "\x8e", // "Ћ"
	["\xd0\x8f"] = "\x8f", // "Џ"

	["\xd1\x92"] = "\x90", // "ђ"
	["\xe2\x80\x98"] = "\x91", // "‘"
	["\xe2\x80\x99"] = "\x92", // "’"
	["\xe2\x80\x9c"] = "\x93", // "“"
	["\xe2\x80\x9d"] = "\x94", // "”"
	["\xe2\x80\xa2"] = "\x95", // "•"
	["\xe2\x80\x93"] = "\x96", // "–"
	["\xe2\x80\x94"] = "\x97", // "—"
	["\xe2\x84\xa2"] = "\x99", // "™"
	["\xd1\x99"] = "\x9a", // "љ"
	["\xe2\x80\xba"] = "\x9b", // "›"
	["\xd1\x9a"] = "\x9c", // "њ"
	["\xd1\x9c"] = "\x9d", // "ќ"
	["\xd1\x9b"] = "\x9e", // "ћ"
	["\xd1\x9f"] = "\x9f", // "џ"

	["\x20"] = "\xa0", // NBSP
	["\xd0\x8e"] = "\xa1", // "Ў"
	["\xd1\x9e"] = "\xa2", // "ў"
	["\xd0\x88"] = "\xa3", // "Ј"
	["\xc2\xa4"] = "\xa4", // "¤"
	["\xd2\x90"] = "\xa5", // "Ґ"
	["\xc2\xa6"] = "\xa6", // "¦"
	["\xc2\xa7"] = "\xa7", // "§"
	["\xd0\x81"] = "\xa8", // "Ё"
	["\xc2\xa9"] = "\xa9", // "©"
	["\xd0\x84"] = "\xaa", // "Є"
	["\xc2\xab"] = "\xab", // "«"
	["\xc2\xac"] = "\xac", // "¬"
	["\xc2\xad"] = "\xad", // SHY
	["\xc2\xae"] = "\xae", // "®"
	["\xd0\x87"] = "\xaf", // "Ї"

	["\xc2\xb0"] = "\xb0", // "°"
	["\xc2\xb1"] = "\xb1", // "±"
	["\xd0\x86"] = "\xb2", // "І"
	["\xd1\x96"] = "\xb3", // "і"
	["\xd2\x91"] = "\xb4", // "ґ"
	["\xc2\xb5"] = "\xb5", // "µ"
	["\xc2\xb6"] = "\xb6", // "¶"
	["\xc2\xb7"] = "\xb7", // "·"
	["\xd1\x91"] = "\xb8", // "ё"
	["\xe2\x84\x96"] = "\xb9", // "№"
	["\xd1\x94"] = "\xba", // "є"
	["\xc2\xbb"] = "\xbb", // "»"
	["\xd1\x98"] = "\xbc", // "ј"
	["\xd0\x85"] = "\xbd", // "Ѕ"
	["\xd1\x95"] = "\xbe", // "ѕ"
	["\xd1\x97"] = "\xbf", // "ї"

	["\xd0\x90"] = "\xc0", // "А"
	["\xd0\x91"] = "\xc1", // "Б"
	["\xd0\x92"] = "\xc2", // "В"
	["\xd0\x93"] = "\xc3", // "Г"
	["\xd0\x94"] = "\xc4", // "Д"
	["\xd0\x95"] = "\xc5", // "Е"
	["\xd0\x96"] = "\xc6", // "Ж"
	["\xd0\x97"] = "\xc7", // "З"
	["\xd0\x98"] = "\xc8", // "И"
	["\xd0\x99"] = "\xc9", // "Й"
	["\xd0\x9a"] = "\xca", // "К"
	["\xd0\x9b"] = "\xcb", // "Л"
	["\xd0\x9c"] = "\xcc", // "М"
	["\xd0\x9d"] = "\xcd", // "Н"
	["\xd0\x9e"] = "\xce", // "О"
	["\xd0\x9f"] = "\xcf", // "П"

	["\xd0\xa0"] = "\xd0", // "Р"
	["\xd0\xa1"] = "\xd1", // "С"
	["\xd0\xa2"] = "\xd2", // "Т"
	["\xd0\xa3"] = "\xd3", // "У"
	["\xd0\xa4"] = "\xd4", // "Ф"
	["\xd0\xa5"] = "\xd5", // "Х"
	["\xd0\xa6"] = "\xd6", // "Ц"
	["\xd0\xa7"] = "\xd7", // "Ч"
	["\xd0\xa8"] = "\xd8", // "Ш"
	["\xd0\xa9"] = "\xd9", // "Щ"
	["\xd0\xaa"] = "\xda", // "Ъ"
	["\xd0\xab"] = "\xdb", // "Ы"
	["\xd0\xac"] = "\xdc", // "Ь"
	["\xd0\xad"] = "\xdd", // "Э"
	["\xd0\xae"] = "\xde", // "Ю"
	["\xd0\xaf"] = "\xdf", // "Я"

	["\xd0\xb0"] = "\xe0", // "а"
	["\xd0\xb1"] = "\xe1", // "б"
	["\xd0\xb2"] = "\xe2", // "в"
	["\xd0\xb3"] = "\xe3", // "г"
	["\xd0\xb4"] = "\xe4", // "д"
	["\xd0\xb5"] = "\xe5", // "е"
	["\xd0\xb6"] = "\xe6", // "ж"
	["\xd0\xb7"] = "\xe7", // "з"
	["\xd0\xb8"] = "\xe8", // "и"
	["\xd0\xb9"] = "\xe9", // "й"
	["\xd0\xba"] = "\xea", // "к"
	["\xd0\xbb"] = "\xeb", // "л"
	["\xd0\xbc"] = "\xec", // "м"
	["\xd0\xbd"] = "\xed", // "н"
	["\xd0\xbe"] = "\xee", // "о"
	["\xd0\xbf"] = "\xef", // "п"

	["\xd1\x80"] = "\xf0", // "р"
	["\xd1\x81"] = "\xf1", // "с"
	["\xd1\x82"] = "\xf2", // "т"
	["\xd1\x83"] = "\xf3", // "у"
	["\xd1\x84"] = "\xf4", // "ф"
	["\xd1\x85"] = "\xf5", // "х"
	["\xd1\x86"] = "\xf6", // "ц"
	["\xd1\x87"] = "\xf7", // "ч"
	["\xd1\x88"] = "\xf8", // "ш"
	["\xd1\x89"] = "\xf9", // "щ"
	["\xd1\x8a"] = "\xfa", // "ъ"
	["\xd1\x8b"] = "\xfb", // "ы"
	["\xd1\x8c"] = "\xfc", // "ь"
	["\xd1\x8d"] = "\xfd", // "э"
	["\xd1\x8e"] = "\xfe", // "ю"
	["\xd1\x8f"] = "\xff", // "я"
}

function utf8_to_windows1251(text)
{
	local result = ""

	for (local i = 0, charSize = 0, textLen = text.len(); i != textLen; i += charSize)
	{
		local char = text[i]

		if (char < 0)
			char += 256

		if(char < 128)
			charSize = 1
		else
		{
			if(char < 224)
				charSize = 2
			else if(char < 240)
				charSize = 3
			else if(char < 248)
				charSize = 4
			else if(char == 252)
			charSize = 5
			else
				charSize = 6
		}

		local charBytes = text.slice(i, i + charSize)

		if (charSize == 1)
			result += charBytes
		else if (charBytes in utf8_windows1251_map)
			result += utf8_windows1251_map[charBytes]
		else
			result += "?"
	}

	return result
}