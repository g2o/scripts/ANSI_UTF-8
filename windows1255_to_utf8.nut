local windows1255_utf8_map =
{
	[0x80] = "\xe2\x82\xac", // "€"
	[0x82] = "\xe2\x80\x9a", // "‚"
	[0x83] = "\xc6\x92", // "ƒ"
	[0x84] = "\xe2\x80\x9e", // "„"
	[0x85] = "\xe2\x80\xa6", // "…"
	[0x86] = "\xe2\x80\xa0", // "†"
	[0x87] = "\xe2\x80\xa1", // "‡"
	[0x88] = "\xcb\x86", // "ˆ"
	[0x89] = "\xe2\x80\xb0", // "‰"
	[0x8b] = "\xe2\x80\xb9", // "‹"

	[0x91] = "\xe2\x80\x98", // "‘"
	[0x92] = "\xe2\x80\x99", // "’"
	[0x93] = "\xe2\x80\x9c", // "“"
	[0x94] = "\xe2\x80\x9d", // "”"
	[0x95] = "\xe2\x80\xa2", // "•"
	[0x96] = "\xe2\x80\x93", // "–"
	[0x97] = "\xe2\x80\x94", // "—"
	[0x98] = "\xcb\x9c", // "˜"
	[0x99] = "\xe2\x84\xa2", // "™"
	[0x9b] = "\xe2\x80\xba", // "›"

	[0xa0] = "\xa0", // NBSP
	[0xa1] = "\xc2\xa1", // "¡"
	[0xa2] = "\xc2\xa2", // "¢"
	[0xa3] = "\xc2\xa3", // "£"
	[0xa4] = "\xe2\x82\xaa", // "₪"
	[0xa5] = "\xc2\xa5", // "¥"
	[0xa6] = "\xc2\xa6", // "¦"
	[0xa7] = "\xc2\xa7", // "§"
	[0xa8] = "\xc2\xa8", // "¨"
	[0xa9] = "\xc2\xa9", // "©"
	[0xaa] = "\xc3\x97", // "×"
	[0xab] = "\xc2\xab", // "«"
	[0xac] = "\xc2\xac", // "¬"
	[0xad] = "\xad", // SHY
	[0xae] = "\xc2\xae", // "®"
	[0xaf] = "\xc2\xaf", // "¯"

	[0xb0] = "\xc2\xb0", // "°"
	[0xb1] = "\xc2\xb1", // "±"
	[0xb2] = "\xc2\xb2", // "²"
	[0xb3] = "\xc2\xb3", // "³"
	[0xb4] = "\xc2\xb4", // "´"
	[0xb5] = "\xc2\xb5", // "µ"
	[0xb6] = "\xc2\xb6", // "¶"
	[0xb7] = "\xc2\xb7", // "·"
	[0xb8] = "\xc2\xb8", // "¸"
	[0xb9] = "\xc2\xb9", // "¹"
	[0xba] = "\xc3\xb7", // "÷"
	[0xbb] = "\xc2\xbb", // "»"
	[0xbc] = "\xc2\xbc", // "¼"
	[0xbd] = "\xc2\xbd", // "½"
	[0xbe] = "\xc2\xbe", // "¾"
	[0xbf] = "\xc2\xbf", // "¿"

	[0xc0] = "\xd6\xb0", // "ְ"
	[0xc1] = "\xd6\xb1", // "ֱ"
	[0xc2] = "\xd6\xb2", // "ֲ"
	[0xc3] = "\xd6\xb3", // "ֳ"
	[0xc4] = "\xd6\xb4", // "ִ"
	[0xc5] = "\xd6\xb5", // "ֵ"
	[0xc6] = "\xd6\xb6", // "ֶ"
	[0xc7] = "\xd6\xb7", // "ַ"
	[0xc8] = "\xd6\xb8", // "ָ"
	[0xc9] = "\x20\xd6\xb9", // " ֹ"
	[0xca] = "\x20\xd6\xba", // " ֺ"
	[0xcb] = "\xd6\xbb", // "ֻ"
	[0xcc] = "\xd6\xbc", // "ּ"
	[0xcd] = "\xd6\xbd", // "ֽ"
	[0xce] = "\xd6\xbe", // "־"
	[0xcf] = "\xd6\xbf", // "ֿ"

	[0xd0] = "\xd7\x80", // "׀"
	[0xd1] = "\xd7\x81", // "ׁ"
	[0xd2] = "\xd7\x82", // "ׂ"
	[0xd3] = "\xd7\x83", // "׃"
	[0xd4] = "\xd7\xb0", // "װ"
	[0xd5] = "\xd7\xb1", // "ױ"
	[0xd6] = "\xd7\xb2", // "ײ"
	[0xd7] = "\xd7\xb3", // "׳"
	[0xd8] = "\xd7\xb4", // "״"

	[0xe0] = "\xd7\x90", // "א"
	[0xe1] = "\xd7\x91", // "ב"
	[0xe2] = "\xd7\x92", // "ג"
	[0xe3] = "\xd7\x93", // "ד"
	[0xe4] = "\xd7\x94", // "ה"
	[0xe5] = "\xd7\x95", // "ו"
	[0xe6] = "\xd7\x96", // "ז"
	[0xe7] = "\xd7\x97", // "ח"
	[0xe8] = "\xd7\x98", // "ט"
	[0xe9] = "\xd7\x99", // "י"
	[0xea] = "\xd7\x9a", // "ך"
	[0xeb] = "\xd7\x9b", // "כ"
	[0xec] = "\xd7\x9c", // "ל"
	[0xed] = "\xd7\x9d", // "ם"
	[0xee] = "\xd7\x9e", // "מ"
	[0xef] = "\xd7\x9f", // "ן"

	[0xf0] = "\xd7\xa0", // "נ"
	[0xf1] = "\xd7\xa1", // "ס"
	[0xf2] = "\xd7\xa2", // "ע"
	[0xf3] = "\xd7\xa3", // "ף"
	[0xf4] = "\xd7\xa4", // "פ"
	[0xf5] = "\xd7\xa5", // "ץ"
	[0xf6] = "\xd7\xa6", // "צ"
	[0xf7] = "\xd7\xa7", // "ק"
	[0xf8] = "\xd7\xa8", // "ר"
	[0xf9] = "\xd7\xa9", // "ש"
	[0xfa] = "\xd7\xaa", // "ת"
	[0xfd] = "\x4c\x52\x4d", // LRM
	[0xfe] = "\x52\x4c\x4d", // RLM
}

function windows1255_to_utf8(text)
{
	local result = ""

	foreach (idx, charId in text)
	{
		charId += 256

		if (charId in windows1255_utf8_map)
			result += windows1255_utf8_map[charId]
		else
			result += text.slice(idx, idx + 1)
	}

	return result
}